package com.example.hellas_direct_ex.model;

public enum FloodRisk
{
    NO_RISK,
    LOW_RISK,
    MEDIUM_RISK,
    HIGH_RISK
}
