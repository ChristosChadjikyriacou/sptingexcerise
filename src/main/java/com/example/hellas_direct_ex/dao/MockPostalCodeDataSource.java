package com.example.hellas_direct_ex.dao;

import com.example.hellas_direct_ex.exceptions.PostcodeNotFoundException;
import com.example.hellas_direct_ex.exceptions.SubsidenceAreaCheckerTechnicalFailureException;
import com.example.hellas_direct_ex.model.FloodRisk;
import com.example.hellas_direct_ex.model.PostalCode;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Repository
@Qualifier("mockData")
public class MockPostalCodeDataSource implements PostalCodeDao {


    private static List<PostalCode> DB = new ArrayList<>();

    @Override
    public List<PostalCode> getAllStudents() {
        return DB;
    }

    @Override
    public PostalCode getPostalCodeByValue(String value) throws Exception {
        return DB
                .stream()
                .filter(p -> p.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new PostcodeNotFoundException());

    }


    @Override
    public void insertStudentToDb(PostalCode postalCode) {
        DB.add(postalCode);
    }



}
