package com.example.hellas_direct_ex.dao;

import com.example.hellas_direct_ex.model.PostalCode;

import java.util.List;
import java.util.Optional;

public interface PostalCodeDao {

    List<PostalCode> getAllStudents();

    PostalCode getPostalCodeByValue(String value) throws Exception;

    void insertStudentToDb(PostalCode postalCode);

}
