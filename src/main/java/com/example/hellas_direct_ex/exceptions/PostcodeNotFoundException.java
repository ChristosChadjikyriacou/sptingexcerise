package com.example.hellas_direct_ex.exceptions;

public class PostcodeNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = 1L;
}
