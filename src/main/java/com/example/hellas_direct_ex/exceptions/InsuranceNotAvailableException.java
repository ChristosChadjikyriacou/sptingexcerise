package com.example.hellas_direct_ex.exceptions;

public class InsuranceNotAvailableException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Insurance is not available";
    }
}
