package com.example.hellas_direct_ex.service;


import com.example.hellas_direct_ex.exceptions.FloodAreaCheckerTechnicalFailureException;
import com.example.hellas_direct_ex.model.FloodRisk;
import com.example.hellas_direct_ex.model.PostalCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FloodAreaCheckerService implements FloodAreaChecker {

    @Autowired
    private PostalCodeService postalCodeService;

    public FloodAreaCheckerService(PostalCodeService postalCodeService) {
        this.postalCodeService = postalCodeService;
    }

    //TODO: Call to the database for the correct values.
    @Override
    public FloodRisk isPostcodeInFloodArea(String postcode) throws Exception {
        PostalCode postalCode = postalCodeService.getPostalCodeByValue(postcode);

        if (postalCode.getValue().endsWith("0")) {
            throw new FloodAreaCheckerTechnicalFailureException();
        }
        else if (postalCode.getValue().endsWith("1")) {
            return FloodRisk.HIGH_RISK;
        }

        else if (postalCode.getValue().endsWith("2")) {
            return FloodRisk.MEDIUM_RISK;
        }

        else if (postalCode.getValue().endsWith("3")) {
            return FloodRisk.LOW_RISK;
        }

        else  {
            return FloodRisk.NO_RISK;
        }

    }
}
