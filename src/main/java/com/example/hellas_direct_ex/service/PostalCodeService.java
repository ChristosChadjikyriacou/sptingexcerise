package com.example.hellas_direct_ex.service;

import com.example.hellas_direct_ex.dao.PostalCodeDao;
import com.example.hellas_direct_ex.model.PostalCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PostalCodeService {

    @Autowired
    private final PostalCodeDao postalCodeDao;


    PostalCodeService(@Qualifier("mockData") PostalCodeDao postalCodeDao) {
        this.postalCodeDao = postalCodeDao;
    }


    public List<PostalCode> getAllStudents() {
        return postalCodeDao.getAllStudents();
    }

    public PostalCode getPostalCodeByValue(String value) throws Exception {
        return postalCodeDao.getPostalCodeByValue(value);
    }

    public void insertStudentToDb(PostalCode postalCode) {
        postalCodeDao.insertStudentToDb(postalCode);
    }


}
