package com.example.hellas_direct_ex.service;

import com.example.hellas_direct_ex.exceptions.SubsidenceAreaCheckerTechnicalFailureException;
import com.example.hellas_direct_ex.model.PostalCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubsidenceAreaCheckerService implements SubsidenceAreaChecker {


    @Autowired
    private PostalCodeService postalCodeService;

    public SubsidenceAreaCheckerService(PostalCodeService postalCodeService) {
        this.postalCodeService = postalCodeService;
    }

    //TODO: Call to the database for the correct values.
    @Override
    public Boolean isPostcodeInSubsidenceArea(String postcode) throws Exception {

        PostalCode postalCode = postalCodeService.getPostalCodeByValue(postcode);

        if (postalCode.getValue().endsWith("9")) {
            throw new  SubsidenceAreaCheckerTechnicalFailureException();
        }
        else if (postalCode.getValue().endsWith("0") ||
                postalCode.getValue().endsWith("1") ||
                postalCode.getValue().endsWith("2") ||
                postalCode.getValue().endsWith("3")) {
            return false;
        }
        else {
            return true;
        }



    }
}
