package com.example.hellas_direct_ex.service;


import com.example.hellas_direct_ex.exceptions.InsuranceNotAvailableException;
import com.example.hellas_direct_ex.model.FloodRisk;
import com.example.hellas_direct_ex.model.PostalCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HouseInsuranceService implements HouseInsurance {


    @Autowired
    private  SubsidenceAreaChecker subsidenceAreaChecker;

    @Autowired
    private FloodAreaChecker floodAreaChecker;


    HouseInsuranceService(
            FloodAreaChecker floodAreaChecker, SubsidenceAreaChecker subsidenceAreaChecker) {
        this.subsidenceAreaChecker = subsidenceAreaChecker;
        this.floodAreaChecker = floodAreaChecker;
    }

    public float getCalculatedPremium(String postcode, int numBedrooms, boolean hasThatchedRoof) throws Exception{

        float baseFee = 110f;

        if (numBedrooms > 4) {
            throw new InsuranceNotAvailableException();
        }

        if (hasThatchedRoof) {
            throw new InsuranceNotAvailableException();
        }


        if (subsidenceAreaChecker.isPostcodeInSubsidenceArea(postcode)) {
            throw new InsuranceNotAvailableException();
        }


        FloodRisk floodRisk = floodAreaChecker.isPostcodeInFloodArea(postcode);

        if (floodRisk == FloodRisk.HIGH_RISK) {
            throw new InsuranceNotAvailableException();
        }

        if (floodRisk == FloodRisk.MEDIUM_RISK) {
            return baseFee + (baseFee * 15) / 100;
        }

        return baseFee;




    }
}
