package com.example.hellas_direct_ex.service;

public interface HouseInsurance
{
    public float getCalculatedPremium(String postcode, int numBedrooms, boolean hasThatchedRoof) throws Exception;
}
