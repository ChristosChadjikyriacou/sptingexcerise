package com.example.hellas_direct_ex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellasDirectExApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellasDirectExApplication.class, args);
	}

}
