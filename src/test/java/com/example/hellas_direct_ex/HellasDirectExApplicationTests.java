package com.example.hellas_direct_ex;

import com.example.hellas_direct_ex.exceptions.FloodAreaCheckerTechnicalFailureException;
import com.example.hellas_direct_ex.exceptions.InsuranceNotAvailableException;
import com.example.hellas_direct_ex.exceptions.PostcodeNotFoundException;
import com.example.hellas_direct_ex.exceptions.SubsidenceAreaCheckerTechnicalFailureException;
import com.example.hellas_direct_ex.model.FloodRisk;
import com.example.hellas_direct_ex.model.PostalCode;
import com.example.hellas_direct_ex.service.HouseInsuranceService;
import com.example.hellas_direct_ex.service.PostalCodeService;
import javafx.geometry.Pos;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.sun.tools.internal.xjc.reader.Ring.add;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HellasDirectExApplicationTests {

	@Autowired
	PostalCodeService postalCodeService;

	@Autowired
	HouseInsuranceService houseInsuranceService;

	@Test
	public void ServicesAutowired() {
		assertNotNull(postalCodeService);
		assertNotNull(houseInsuranceService);
	}


	ArrayList<PostalCode> mockPostalCodes;

	@Before
	public void init() {
		this.mockPostalCodes = new ArrayList<PostalCode>() {{
			add(new PostalCode("1230"));
			add(new PostalCode("1231"));
			add(new PostalCode("1232"));
			add(new PostalCode("1233"));
			add(new PostalCode("1234"));
			add(new PostalCode("1239"));

		}};


		for (PostalCode pc:this.mockPostalCodes) {
			postalCodeService.insertStudentToDb(pc);
		}

	}

	@Test
	public  void PostalCodeService() {

		assertEquals(mockPostalCodes.size(),postalCodeService.getAllStudents().size());

		try {
			assertEquals(postalCodeService.getPostalCodeByValue("1231"), this.mockPostalCodes.get(0));
		}
		catch (Exception ex) {
			fail(ex.getMessage());
		}

	}


	@Test
	public  void HouseInsuranceService() {
		try {
			houseInsuranceService.getCalculatedPremium("9999",1,false);
			fail("Expected PostcodeNotFoundException exception");
		}catch (Exception e) {
			assertThat(e,instanceOf(PostcodeNotFoundException.class));
		}

		try {
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(0).getValue(),1,false);
			fail("Expected FloodAreaCheckerTechnicalFailureException exception");
		}catch (Exception e) {
			assertThat(e,instanceOf(FloodAreaCheckerTechnicalFailureException.class));
		}

		try {
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(5).getValue(),1,false);
			fail("Expected SubsidenceAreaCheckerTechnicalFailureException exception");
		}catch (Exception e) {
			assertThat(e,instanceOf(SubsidenceAreaCheckerTechnicalFailureException.class));
		}


		try{
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(1).getValue(),1,true);
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(2).getValue(),1,true);
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(3).getValue(),1,true);
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(4).getValue(),1,true);

			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(1).getValue(),5,false);
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(2).getValue(),5,false);
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(3).getValue(),5,false);
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(4).getValue(),5,false);


			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(4).getValue(),1,false);
			houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(1).getValue(),1,false);


			fail("Expected InsuranceNotAvailableException exception");
		} catch(Exception e) {
			assertThat(e, instanceOf(InsuranceNotAvailableException.class));
		}

		try {


			assertEquals(houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(2).getValue(),1,false), 110f + (110f * 15) / 100,0f);
			assertEquals(houseInsuranceService.getCalculatedPremium(this.mockPostalCodes.get(3).getValue(), 1, false),110f,0f);

		}catch (Exception ex) {
			fail(ex.getMessage());
		}


	}


	@After
	public void finalize() {
		mockPostalCodes.clear();
	}






}
